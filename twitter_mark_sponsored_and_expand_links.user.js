// ==UserScript==
// @name twitter_mark_sponsored_and_expand_links
// @match https://twitter.com/*
// @require https://code.jquery.com/jquery-3.3.1.min.js
// @grant none
// ==/UserScript==

const mark_sponsored_and_convert_links = tweet_jquery => {
  // mark sposonred
  tweet_jquery
    .filter((index, element) => $(element).children(".promoted-tweet").length !== 0)
    .css({"background-color": "pink"});
  // convert links
  tweet_jquery.find("a[data-expanded-url]").each((index, element) => {
    const element_jquery = $(element);
    element_jquery.prop("href", element_jquery.data("expanded-url"));
  });
};
const stream = $("ol#stream-items-id").get(0);
mark_sponsored_and_convert_links($("li.stream-item"));
(
  new MutationObserver(mutation => {
    mark_sponsored_and_convert_links(
      $(
        mutation
          .map(mutation_current => Array.from(mutation_current.addedNodes))
          .flat()
      )
    );
  })
).observe($("ol#stream-items-id").get(0), {"childList": true});
